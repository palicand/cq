#!/usr/bin/env bash

# consistency-tests.bash
#
# === What is this? ===
# These tests try current version of cq against known pieces of code, so we can have greater control of how different,
# version of pyflakes or mypy affects the resulting messages.
#
# === What it does? ===
# It runs cq on Python files (.py) present in `samples` folder and compares the output with file with same name, but
# with different ending (.out). Be aware, that these files has to be stored in `samples` directory, without any
# subdirectories.
#
# === How to run this? ===
# Simply execute this file in root folder of this (meta/cq) project. cq must be hidden in folder named 'cq' and folder
# 'samples' must also be present in a root directory of this project.

echo "cq consistency tests"
echo "===================="

EXIT_CODE=0

for PYTHON_FILE in samples/*.py; do
    OUTPUT_FILE="$(echo $PYTHON_FILE| cut -d. -f1 ).out"

    if [[ ! -f $OUTPUT_FILE ]]; then
        echo "$PYTHON_FILE: missing $OUTPUT_FILE, FAIL"
        EXIT_CODE=1
        continue
    fi

    TEST_RESULT=`PYTHONPATH=. python cq/main.py $PYTHON_FILE`

    echo -n "$PYTHON_FILE: "

    EXPECTED_RESULT=`< $OUTPUT_FILE`
    if [[ "$EXPECTED_RESULT" != "$TEST_RESULT" ]]; then
        echo "FAIL"

        diff <(echo "$EXPECTED_RESULT") <(echo "$TEST_RESULT")

        EXIT_CODE=1
    else
        echo "OK"
    fi
done

exit $EXIT_CODE
