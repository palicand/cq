import os

import pytest

import cq.main



@pytest.fixture
def cli_runner():
	from click.testing import CliRunner
	return CliRunner()


def test_default_modules(cli_runner):
	'''
	Cannot use tmpdir fixture - we need to mock cwd
	'''
	with cli_runner.isolated_filesystem():
		with open('test.py', 'w') as f:
			f.write('myVar = True\n')

		result = cli_runner.invoke(cq.main.main, [])
		assert result.exit_code == 0
		assert result.output == '✓\n'


def test_result_output(cli_runner):
	with cli_runner.isolated_filesystem():
		with open('test.py', 'w') as f:
			f.write('')

		result = cli_runner.invoke(cq.main.main, [os.path.abspath(f.name)])
		assert result.exit_code == 0
		assert result.output == '✓\n'
