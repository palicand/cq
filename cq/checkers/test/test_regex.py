import pytest

import cq.checkers.regex



def _check_grammar_nazi(s: str):
	return list(cq.checkers.regex.check_line(s, s, cq.checkers.regex.GrammarNaziChecker.RULES))


def test_exists():
	assert _check_grammar_nazi("Yo dawg, dis is Queen's English!") == []
	assert _check_grammar_nazi('Trade does NOT exists') == [ # grammar_checker: disable = 3rd-person-singular
		'Use "does not exist", not "does not exists". ' # grammar_checker: disable = 3rd-person-singular
		'The "does" verb already is in third person singular.'
	]
	assert _check_grammar_nazi('Trade may not exists') == [ # grammar_checker: disable = 3rd-person-singular
		'Use "may not exist", not "may not exists". ' # grammar_checker: disable = 3rd-person-singular
		'The "may" verb already is in third person singular.'
	]


def test_happend(): # grammar_checker: disable = happend
	assert _check_grammar_nazi('This can happend only ...') == [ # grammar_checker: disable = happend
		'"happend" is not a word. ' # grammar_checker: disable = happend
		'You either mean "happen" (present tense) or "happened" (past tense).'
	]


def test_it_self():
	assert _check_grammar_nazi('await self.x()') == []


def test_single():
	assert cq.checkers.regex.clobber_string_literals("print('hi')") == "print('..')"


def test_double():
	assert cq.checkers.regex.clobber_string_literals('print("hi")') == 'print("..")'


def test_escape_single():
	assert cq.checkers.regex.clobber_string_literals(''' q = '\\'' ''') == " q = '..' "


def test_escape_double():
	assert cq.checkers.regex.clobber_string_literals(''' q = "\\"" ''') == ' q = ".." '


def _check_dumb_style_checker(s: str):
	return list(cq.checkers.regex.check_line(s, s, cq.checkers.regex.DumbStyleChecker.RULES))


@pytest.mark.parametrize('source', (
	'a = 1',
	'a == 1',
	'a >= 1',
	'a <= 1',
	'a != 1',
	'a += 1',
	'a =',
))
def test_spaces_around_equals_sign_negative(source):
	assert _check_dumb_style_checker(source) == []


@pytest.mark.parametrize('source', (
	'print_this("hi", end="!")',
	'a=1',
	'a =1',
	'a= 1',
))
def test_spaces_around_equals_sign_positive(source):
	result = _check_dumb_style_checker(source)
	assert len(result) == 1
	assert result[0].startswith('Put exactly one space before and after `=`')


@pytest.mark.parametrize('source, operator', (
	('a!=1', '!='),
	('a !=1', '!='),
	('a>=1', '>='),
	('a== 1', '=='),
))
def test_spaces_around_other_operators_positive(source, operator):
	result = _check_dumb_style_checker(source)
	assert len(result) == 1
	assert result[0].startswith(f'Put exactly one space before and after `{operator}`')


@pytest.mark.parametrize('source', (
	'variable1 = ...',
	'variable2 = ...',
	'variable4 = ...',
	# These lines should be ignored, by disabling them
	'OAuth2Authenticator # dumb_style_checker:disable = word_as_a_digit',
	'oauth2client.client # dumb_style_checker: disable = word_as_a_digit',
))
def test_number_in_names_negative(source):
	assert _check_dumb_style_checker(source) == []


@pytest.mark.parametrize('source, number', (
	('def convert2string() -> str:', '2'),
	('def test4missing_rules():', '4'),
	('convert2price = []', '2'),
))
def test_number_in_names_positive(source, number):
	result = _check_dumb_style_checker(source)
	assert len(result) == 1

	verb = 'to' if number == '2' else 'for'
	assert result[0].startswith(f'Do not spell `{verb}` as the digit `{number}`')


@pytest.mark.parametrize('source', (
	'some_print(some_stuff)',
	'print(with_disable) # dumb_style_checker:disable = print-statement',
))
def test_print_statement_negative(source):
	assert _check_dumb_style_checker(source) == []


@pytest.mark.parametrize('source', (
	' print(some_stuff)',
	' print("other stuff")',
))
def test_print_statement_positive(source):
	result = _check_dumb_style_checker(source)
	assert len(result) == 1
	assert result[0].startswith(
		'It seems that you have forgotten about a print statement. '
		'If this is intentional, disable this check by # dumb_style_checker:disable = print-statement'
	)
