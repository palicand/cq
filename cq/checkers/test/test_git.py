import os

import pytest

import cq.checkers.git
import cq.utils


@pytest.fixture
def branch_name_checker():
	return cq.checkers.git.BranchNameChecker()



def test_correct_branch_name_env(monkeypatch, branch_name_checker):
	monkeypatch.setattr(os, 'environ', {'CI_COMMIT_REF_NAME': 'feature/cool_new_feature'})

	result = branch_name_checker.run([])
	assert result.return_code == 0



def test_correct_branch_name_git(monkeypatch, branch_name_checker):
	def runner(cmd, _options):
		if cmd != 'git':
			assert False

		return ('master', 0)

	monkeypatch.setattr(cq.utils, 'run_external_checker', runner)

	result = branch_name_checker.run([])
	assert result.return_code == 0



def test_incorrect_branch_name_env(monkeypatch, branch_name_checker):
	monkeypatch.setattr(os, 'environ', {'CI_COMMIT_REF_NAME': 'incorrectly_named_branch'})

	result = branch_name_checker.run([])
	assert result.return_code == 1
